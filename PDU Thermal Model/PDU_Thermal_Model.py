print ("Input electrical parameters")
I = input("Enter the current through PDU in amperes: ")
I = int(I)
#print ("Current through PDU")
#print(I)
#Rds = input("Enter Rds on of MOSFET in miliohms: ")
Rds = 3.5
print ("MOSFET Rdson Max")
print(Rds)
#Rjunctiontocase = input("Enter thermal resistance of MOSFET's junction to case in degC/W: ")
Rjunctiontocase = 0.88
print ("MOSFET's junction to case thermal resistance in degC/W")
print(Rjunctiontocase)
#Rjunctiontoamb = input("Enter thermal resistance of MOSFET's junction to ambient in degC/W: ")
Rcasetoamb = 50
print ("MOSFET's case to ambient thermal resistance in degC/W")
print(Rcasetoamb)
#Mosparallel = input("Enter no of MOSFET in parellel: ")
Mosinparallel = 6
print ("Total number of Mosfets in Parellel")
print(Mosinparallel)
#Mosinseries = input("Enter no of MOSFET in series: ")
Mosinseries = 2
print ("Total number of Mosfets in Series")
print(Mosinseries)

print ("Board parameters")
#Vias = input("Enter no of Vias under MOSFET: ")
Vias = 25
print ("Vias under single MOSFET ")
print(Vias)
#Lmos = input("Enter MOSFET Length in cm: ")
Lmos = 0.515*6
print ("Total MOSFET length in CM")
print(Lmos)
#Wmos = input("Enter MOSFET width in cm: ")
Wmos = 0.714*2
print ("Total MOSFET width in CM")
print(Wmos)
#Lboard = input("Enter PCB length Considered for 1 MOS in cm: ")
Lboard = 0.9027*6
print ("Total PDU length in CM")
print(Lboard)
#Wboard = input("Enter PCB width Considered for 1 MOS in cm: ")
Wboard = 1.1325*2
print ("Total PDU width in CM")
print(Wboard)
#PCBlayers = input("Enter No of layers on PCB: ")
PCBlayers = 4
print ("Number of PCB layers ")
print(PCBlayers)
#Cuthickness = input("Enter copper thickness of PCB in cm: ")
Cuthickness = 0.007
print ("Copper thickness on single layer in CM ")
print(Cuthickness)
#Vialength = input("Enter via length in cm: ")
Vialength = 0.165
print ("Via length in CM ")
print(Vialength)
#Fr4thickness = input("Enter Fr4 thickness in cm: ")
Fr4thickness = 0.044
print ("FR4 thickness in CM ")
print(Fr4thickness)


#Tamb = input("Enter Ambient temperature in degC: ")
Tamb = 25
print ("Ambient Temperature")
print(Tamb)

#print("Internal parameters")
rocu = 1.7*10**(-6)
Cutempcoef = 3.9*10**(-3)
Cuthermalconductivity = 3.85
Vialength = 0.165
Viathickness = 0.00175
Viaradius = 0.01524
Fr4conductivity = 0.0023
h = 0.001511



print("Electrical Power Loss in Watts")
Rdseq = Rds*(Mosinseries/Mosinparallel)
Rcu = rocu*(Lboard/(Cuthickness*Wboard))*(1+Cutempcoef*(Tamb-25))
Rtotal = (Rdseq*10**(-3))+(Rcu/PCBlayers)
#print(Rtotal)
Ploss = ((I)**2)*(Rtotal)
print(Ploss)


print("Thermal Conduction in Watts")

Rboardlayer = (1/Cuthermalconductivity)*Cuthickness/((Wmos)*(Lmos))
#print (Rboardlayer)
Rfr4 = (1/Fr4conductivity)*(Fr4thickness /((Wmos)*(Lmos)))
#print (Rfr4)
Rvia = (1/Cuthermalconductivity)*(Vialength)/(3.1428*((Viaradius**2)-(Viaradius-Viathickness)**2))
#print (Rvia)

Rboardlayernonmos = (1/Cuthermalconductivity)*Cuthickness/((Wboard-Wmos)*(Lboard-Lmos))
#print (Rboardlayernonmos)
Rfr4nonmos = (1/Fr4conductivity)*(Fr4thickness /((Wboard-Wmos)*(Lboard-Lmos)))
#print (Rfr4nonmos)

Rconduction =(((4*Rboardlayer+3*Rfr4)*(Rvia/(Vias*Mosinparallel*Mosinseries))/((4*Rboardlayer+3*Rfr4)+(Rvia/(Vias*Mosinparallel*Mosinseries)))))*(Rfr4nonmos+Rboardlayernonmos)/(((4*Rboardlayer+3*Rfr4)*(Rvia/(Vias*Mosinparallel*Mosinseries))/((4*Rboardlayer+3*Rfr4)+(Rvia/(Vias*Mosinparallel*Mosinseries))))+(Rfr4nonmos+Rboardlayernonmos))
print (Rconduction)


print("Thermal Convection in Watts")
Rconvectionfromtop = 1/(h*(Wboard-Wmos)*(Lboard-Lmos))
Rconvectionfrombot = 1/(h*(Wboard)*(Lboard))

print (Rconvectionfromtop)
print (Rconvectionfrombot)

Rconvection = (Rconvectionfromtop * (Rconduction+Rconvectionfrombot))/(Rconvectionfromtop+(Rconduction+Rconvectionfrombot))
print("Thermal Convection from board in Watts")
print (Rconvection)


print("Total thermal Resistor Rja")
Rja = Rcasetoamb*(Rconvection)/(Rcasetoamb+Rconvection)
print(Rja)

print("Total Change in Component temperature")
DeltaT = Ploss*Rja
print(DeltaT)
